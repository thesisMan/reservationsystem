<?php
	include("db.php");
	if(!isset($_GET['id'])){
		header("Location:AddFeed.php");
	}
	$id = $_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lumino - Dashboard</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/DataTable.min.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>
<style>
	table {
	 	table-layout:fixed; word-break: break-all;
	 }
	td{
		padding: 10px;
		text-align: right;
		overflow:hidden;
	}
	.modal-backdrop, .modal-dialog {
  	 background-color: #b3b3b3;
	}
	.modal-content {
		background-color: #666666;
	}
	.panel-body{
		background-color: #d9d9d9; 
	}
</style>

<body onload="myFunction()">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="admin.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="widgets.html"><svg class="glyph stroked empty-message"><use xlink:href="#stroked-empty-message"></use></svg> Inbox</a></li>
			<li><a href="charts.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Add Account</a></li>
			<li class="active"><a href="AddFeed.php"><svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"> </use></svg>Add Feed</a></li>
		
			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Dropdown 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 1
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 2
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 3
						</a>
					</li>
				</ul>
			</li>
			<li role="presentation" class="divider"></li>
			<li><a href="login.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>
		<div class="attribution">Template by <a href="http://www.medialoot.com/item/lumino-admin-bootstrap-template/">Medialoot</a><br/><a href="http://www.glyphs.co" style="color: #333;">Icons by Glyphs</a></div>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row--><br><br>
		<form action="EditProductProcess.php" method="post">
			<div class="panel panel-info">
				<div class="panel-heading">
					Edit Products
					<div class="pull-right clearfix">
							<button class="btn btn-info" type="submit" name="submit"><span class="glyphicon glyphicon-ok"></span></button>
								<button class="btn btn-default" type="button"><a href="AddFeed.php"><span class="glyphicon glyphicon-remove"></span></a></button>
					</div>
				</div>
					<div class="panel-body">
						<div class="col-md-3">
						</div>
							<div class='col-md-6'>
								<?php
									$query = "SELECT * FROM products WHERE product_id = $id";
									$resultQuery = $conn->query($query);
									$row = $resultQuery->fetch_assoc();
								?>
								<input type="hidden" name="id" value="<?php echo $row['product_id']?>" class="form-control">
										<br><center><b>Product</b></center>
										<input type="text" name="product" value="<?php echo $row['product']?>" class="form-control">
										<br><center><b>Price</b></center>
										<input type="text" name="price" value="<?php echo $row['price']?>" class="form-control">
										<br><center><b>Quantity</b></center>
										<input type="text" name="quantity" value="<?php echo $row['quantity']?>" class="form-control">
										<br><center><b>Category</b></center>
											<select onClick="isCake()" id="cakeOnly" name="category" class="form-control">
											<?php
												$categoryQuery = "SELECT * FROM category";
												$categoryResult = $conn->query($categoryQuery);
												if($categoryResult->num_rows > 0){
													while($rows = $categoryResult->fetch_assoc()){
														
											?>
														<option <?php 
															if($row['category'] == $rows['category']){
														?>
															 selected
														 <?php
														 	}
														 ?>><?php echo $rows['category'];?></option>
											
											<?php
									
												}
											}
											?>
											</select>
											<span id="displayCake">
												<br><center><b>Shape</center></b>
												<select class="form-control" name="shape">
														<option <?php if($row['shape'] == "Circle"){ ?> selected <?php
															}
														?>>
														Circle
														</option>
								        				<option <?php if($row['shape'] == "Square"){ ?> selected <?php
								        					}
								        				?>>Square</option>
								        				<option <?php if($row['shape'] == "Triangle"){ ?> selected<?php
								        					}
								        				?>>Triangle</option>
												</select>
											</span>
										<br><center><b>Description</b></center>
											<textarea style="resize:none;" rows="5" class="form-control" name="desc"><?php echo $row['description'];?></textarea>
							</div>
								<div class="col-md-3">
								</div>
					</div>	
						
			</div>	
		</form>
		</div><!--/.row-->
	</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/DataTable.min.js"></script>
	<script>
		function isCake(){
			var displayShape = document.getElementById("displayCake");
			var cake = document.getElementById("cakeOnly").value;
			if(cake == "Cakes"){
				displayShape.style.display = "block";
			}else{
				displayShape.style.display = "none";
			}
		}
		function myFunction(){
			var displayShape = document.getElementById("displayCake");
			var cake = document.getElementById("cakeOnly").value;
			if(cake == "Cakes"){
				displayShape.style.display = "block";
			}else{
				displayShape.style.display = "none";
			}
		}

	</script>
</body>

</html>
