<?php 
include("db.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lumino - Dashboard</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="index.html"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="orders.php"><svg class="glyph stroked stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> Orders</a></li>
			<li><a href="inbox.php"><svg class="glyph stroked empty-message"><use xlink:href="#stroked-empty-message"></use></svg> Inbox</a></li>
			<li><a href="charts.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Add Account</a></li>
			<li><a href="tables.html"><svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"> </use></svg>Add Feed</a></li>
			<li role="presentation" class="divider"></li>
			<li><a href="login.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>
		<div class="attribution">Template by <a href="http://www.medialoot.com/item/lumino-admin-bootstrap-template/">Medialoot</a><br/><a href="http://www.glyphs.co" style="color: #333;">Icons by Glyphs</a></div>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
	





<?php
if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `inbox` WHERE CONCAT(`id`, `message`, `date_sent`, `time_sent`, `sender`)
      LIKE '%".$valueToSearch."%'" ;
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `inbox`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "pos");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}


?>






		

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>
        Mailbox
        <small>*Database count* messages</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
             
            </div>
            <div class="panel panel-default">
            <div class="panel header">
            <h2>&nbsp; Messages</h2>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
              <li><button type="button" class="btn btn-lg btn-block btn-default" data-toggle="modal" data-target="#sendMessage">Compose</button></i>
                <li><a href="#"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right">12</span></a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
                <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
                </li>
                <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li><br>
              </ul>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
 
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="panel panel-default">
          <div class="box box-primary">
            <div class="box-header with-border">
            <div class="panel header">
              <h2>&nbsp; Inbox</h2></div>
              <div class="box-tools pull-right">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
<!--                   <tbody>
                    <?php while($row = mysqli_fetch_array($search_result)):?> 
                  <tr>
                    <td><input type="checkbox"></td>
                    <td class="mailbox-name"><a href="read-message.php"><?php echo $row['sender']; ?></a></td>
                    <td class="mailbox-subject"><?php echo $row['message']; ?></td>
                    <td class="mailbox-attachment"></td>
                    <td class="mailbox-date"><b><?php echo $row['date_sent']; ?> - <?php echo $row['time_sent']; ?></b></td>
                  </tr>
                  </tbody>
              <?php endwhile; ?> -->



              

              <tbody>
                <tr>
                  <th>Sender</th>
                  <th>Message</th>
                  <th>Date Sent</th>
                  <th>Time Sent</th>
                </tr>
                <tr>
                <?php
                $inboxselect = "SELECT * FROM INBOX where receiver='kimbryan' "; 
                $inboxresult = $conn->query($inboxselect);
                while ($inboxgetter = mysqli_fetch_assoc($inboxresult)) 
                {
                 ?>
                  <td><?php echo $inboxgetter['sender']; ?></td>
                  <td>
                  <?php echo $inboxgetter['message']; ?>
                  
                  <td><?php echo $inboxgetter['date_sent']; ?></td>
                  <td><?php echo $inboxgetter['time_sent']; ?></td>
                 <?php
                }
                ?>

                </tr>
              </tbody>


















                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
</div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

		</div><!--/.row-->
	</div>	<!--/.main-->















<div class="modal fade" id="sendMessage" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
    <center><h3>Compose Message</h3></center>
        <div class="modal-body">


        <form action="" method="POST">
        <div class="row">
        <div class="container">
        <table width="50%">
<tr>
<td><label>To:</label></td>
<td>
<select name="recipient" class="form-control">
<option placeholder="To:"></option>


<?php

$selectingclients = "SELECT * FROM user_account where access_level ='1' ";
$result = $conn->query($selectingclients);
while ($getter = mysqli_fetch_assoc($result)) 
{
?>
  <option><?php echo $getter['username'];?></option>
<?php
}
 ?>
</select>
</td>
</tr>
</table>
</div>
</div>
<br>
<textarea type="text" name="message" id="editQty" class="form-control" rows="10" cols="20" placeholder="Write message..."></textarea><br>
<input type="submit" name="send" value="Send" class="btn btn-default btn-md">
        </form>
<p id="quantity"></p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php 

if (isset($_POST['send'])) 
{
  $receiver = $_POST['recipient'];
  $message1 = $_POST['message'];

 $inserting = "INSERT into inbox (message,date_sent,time_sent,sender,receiver) values ('$message1',CURRENT_DATE(),CURRENT_TIME(),'ADMIN','$receiver') ";
$result = $conn->query($inserting);
echo "WELLPLAYED";
}
?>







	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
