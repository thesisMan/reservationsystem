<?php
	include("db.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/DataTable.min.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>
<style>
	table {
	 	table-layout:fixed; word-break: break-all;
	 }
	td{
		padding: 10px;
		text-align: center;
		overflow:hidden;
	}
	th{
		text-align: center;
	}
	.modal-backdrop, .modal-dialog {
  	 background-color: #b3b3b3;
	}
	.modal-content {
		background-color: #666666;
	}
</style>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="admin.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
			<li><a href="#"><svg class="glyph stroked empty-message"><use xlink:href="#stroked-empty-message"></use></svg> Inbox</a></li>
			<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Add Account</a></li>
			<li class="active"><a href="AddFeed.php"><svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"> </use></svg>Add Feed</a></li>
		
			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Dropdown 
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 1
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 2
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 3
						</a>
					</li>
				</ul>
			</li>
			<li role="presentation" class="divider"></li>
			<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Feeds</h1>
			</div>
		</div><!--/.row-->
		
		
<div class="panel panel-info">
	<div class="panel-heading">
		Products
		<div class="pull-right">
			<button class="btn btn-info" data-toggle="modal" data-target="#addProduct">Add Product</button>
				<button class="btn btn-info" data-toggle="modal" data-target="#addCategories">Add Category</button>
		</div>
	</div>
		<div class="panel-body">
			<div class="col-md-12">
				<table id="myTable" class="table table-bordered">
					<thead>
						<tr>
							<th>Image</th>
							<th>Product</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>Description</th>
							<th>Category</th>
							<th>Shape</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$productQuery = "SELECT * FROM products";
						$productResult = $conn->query($productQuery);
							if($productResult->num_rows > 0){
								while($rows = $productResult->fetch_assoc()){
					?>
						<tr>
							<td><img src="<?php echo $rows['image']?>" style="width:100px; height:100px;"></td>
							<td><?php echo $rows['product'];?></td>
							<td><?php echo $rows['quantity'];?></td>
							<td><?php echo $rows['price'];?></td>
							<td><?php echo $rows['description'];?></td>
							<td><?php echo $rows['category'];?></td>
							<td><?php echo $rows['shape'];?></td>
							<td>
								<a href="EditProduct.php?id=<?php echo $rows['product_id'];?>"><span class="glyphicon glyphicon-pencil"></span></a>
									<a href="" onClick="deleteProd('<?php echo $rows['product_id']; ?>')" data-toggle="modal" data-target="#deleteProduct"><span class="glyphicon glyphicon-remove"></span></a>
							</td>
						</tr>
					<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>	
						
</div>
		</div><!--/.row-->
	</div>	<!--/.main-->
	<div id="addProduct" class="modal fade" role="dialog">
	<form action="InsertProduct.php" method="post" enctype="multipart/form-data">
	  <div class="modal-dialog modal-sm">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4><center><font color="white">Choose Category</font></center></h4>
	     		<div class="col-md-3">
	     		</div>
	     			<div class="col-md-6">
	        			<select class="form-control" name="category" id="selectCategory" onClick="showCategory()">
	        				<?php
										$getCategory = "SELECT * FROM category";
										$categoryResult = $conn->query($getCategory);
										if($categoryResult->num_rows > 0){
											while($rows = $categoryResult->fetch_assoc()){
									?>
										<option><?php echo $rows['category'];?></option>
									<?php
										}
									}
									?>
	        			</select>
	        		</div>
	        	<div class="col-md-2">
	     		</div><br><br>
	      </div>
	      <div class="modal-body">
		        		<input placeholder="Product Name" type="text" class="form-control" name="product" required>
		        		<br>
		        			<input placeholder="Quantity" type="number" class="form-control" name="quantity" required><br>
		        				<input placeholder="Price" type="number" class="form-control" name="price" required><br>
		        					<textarea rows="5" class="form-control" placeholder="Description" style="resize:none;" name="desc" required></textarea><br>
			        					<font font color="white"><input type="file" name="image" required></font><br>
			        						<span style="display:none;"id="cakeShape"><font font color="white">Pick a Shape:</font>
				        						<select class="form-control" name="shape">
							        				<option>Circle</option>
							        				<option>Square</option>
							        				<option>Triangle</option>
						        				</select><br>
					        					</span>
		        		
	      </div>
	      <div class="modal-footer">
	      	<button type="submit" name="submit" class="btn btn-info">Add</button>
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	  </form>
	</div>
	<div id="addCategories" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">
	  	<form action="NewCategory.php" method="post">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"><font color="white">New Category</font></h4>
	      </div> 
	      <div class="modal-body">
	        <input placeholder="Category" type="text" class="form-control" name="category"> 
	      </div>
	      <div class="modal-footer">
	      	<button type="submit" name="submit" class="btn btn-info">Submit</button>
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	</form>
	  </div>
	</div>

	<div id="deleteProduct" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">
	  	<form action="DeleteProduct.php" method="post">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        
	      </div>
	      <div class="modal-body">
	      	<input type="hidden" id="prodID" name="id">
	        <p><font color="white">Are you sure you want to delete</font></p>
	      </div>
	      <div class="modal-footer">
	      	<button type="submit" name="submit" class="btn btn-danger">Ok</button></a>
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	</form>
	  </div>
	</div>

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/DataTable.min.js"></script>
	<script>
		function showCategory(){
			var categoryName = document.getElementById("selectCategory").value;
			var cakeCategory = document.getElementById("cakeShape");

			if(categoryName == 'Cakes'){
				cakeCategory.style.display="block";
			}else{
				cakeCategory.style.display="none";
			}
		}

		function deleteProd(id){
			$('#prodID').val(id);
		}

		$(document).ready(function(){
   			 $('#myTable').DataTable();
		});
	</script>
</body>

</html>
