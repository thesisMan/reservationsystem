<div id="addImage" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <form action="si.php" method="post" enctype="multipart/form-data">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please select image</h4>
      </div>
      <div class="modal-body">
        <center><input type="file" name="image" required/></center>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" name="submit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  </div>
</div>



<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "pos";

	$conn = new mysqli($servername, $username, $password, $dbname);
	if($conn->connect_error){
		die("connection failed: ".$conn->connect_error);
	}  


	if(isset($_POST['submit'])){
		$uploadDir = "uploadImage/";
		$uploadfile = $uploadDir.basename($_FILES['image']['name']);

		if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)){
			$sql = "INSERT INTO web_cover (image) VALUES ('$uploadfile')";
			$validation = "SELECT * FROM web_cover WHERE image = '$uploadfile'";
			$checkSize = "SELECT COUNT(*) as total FROM web_cover";
			$result = $conn->query($validation);
			$sizeResult = $conn->query($checkSize);
			$getSize = $sizeResult->fetch_assoc();
			if($result->num_rows > 0){
				echo '<script>alert("Image already exist!"); window.location.href="si.php";</script>';
			}else{
				if($getSize['total'] >= 3){
					echo '<script>alert("Image limit exceed!"); window.location.href="si.php";</script>';
				}else{
					if($conn->query($sql) == TRUE){
						echo '<script>alert("Successfully added!"); window.location.href="si.php";</script>';
					}else{
						echo "Error ".$conn->error;
					}
				}
			}

			}
			$conn->close();
		}
?>
